import subprocess

import click


@click.command()
@click.argument("arguments", nargs=-1)
def test(arguments):
    """
    Run all unittests.

    Equivalent to: `poetry run python -m unittest discover`
    """
    subprocess.run(["python", "-m", "unittest", "discover", *arguments])
