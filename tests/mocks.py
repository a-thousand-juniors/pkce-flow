from pkce_flow import AbstractBasePKCEFlowManager

mock_client_id = "mock client id"
mock_client_secret = "mock client secret"
mock_oauth_code = "mock oauth code"
mock_redirect_uri = "https://mock.redirect.uri/receive/"
mock_base_url = "https://mock.base.uri/oauth/"
mock_scopes = "read_api"

# using a string here for simplicity
# in production the user should be an instance of a custom user class
mock_user = "mock@user.com"

mock_state = "mock state"
mock_verifier = "mock code verifier"
mock_challenge = "mock code challenge"


class MockPKCEFlowManagerWithAbstractMethods(AbstractBasePKCEFlowManager):
    base_url = mock_base_url
    client_id = mock_client_id
    client_secret = mock_client_secret
    redirect_uri = mock_redirect_uri
    scope = mock_scopes


class MockPKCEFlowManager(AbstractBasePKCEFlowManager):
    base_url = mock_base_url
    client_id = mock_client_id
    client_secret = mock_client_secret
    redirect_uri = mock_redirect_uri
    scope = mock_scopes

    def store_user_secrets(self, *, user, state, code_verifier, code_challenge):
        ...

    def retrieve_user_state(self, user):
        ...

    def retrieve_user_code_challenge(self, user):
        ...

    def retrieve_user_code_verifier(self, user):
        ...

    def check_user_state(self, *, user, state):
        ...


class MockImproperlyConfiguredPKCEFlowManager(AbstractBasePKCEFlowManager):
    # the required class attributes aren't configured

    def store_user_secrets(self, *, user, state, code_verifier, code_challenge):
        ...

    def retrieve_user_state(self, user):
        ...

    def retrieve_user_code_challenge(self, user):
        ...

    def retrieve_user_code_verifier(self, user):
        ...

    def check_user_state(self, *, user, state):
        ...
