from unittest import TestCase, mock
from urllib.parse import parse_qs, urlparse, urlunparse

import requests
import requests_mock

from pkce_flow.exceptions import ImproperlyConfigured, PKCEFlowError, StateForgeryError
from tests.mocks import (
    MockImproperlyConfiguredPKCEFlowManager,
    MockPKCEFlowManager,
    MockPKCEFlowManagerWithAbstractMethods,
    mock_challenge,
    mock_oauth_code,
    mock_state,
    mock_user,
    mock_verifier,
)


class TestAbstractPKCEFlowManager(TestCase):
    def test_cannot_instantiate_without_overriding_abstract_methods(self):
        with self.assertRaises(TypeError) as cm:
            MockPKCEFlowManagerWithAbstractMethods()

        error_message = str(cm.exception)

        self.assertIn("store_user_secrets", error_message)
        self.assertIn("retrieve_user_state", error_message)
        self.assertIn("retrieve_user_code_challenge", error_message)
        self.assertIn("retrieve_user_code_verifier", error_message)
        self.assertIn("check_user_state", error_message)


mock_pkce_flow_manager = MockPKCEFlowManager()


class TestPKCEFlowManager(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.manager = mock_pkce_flow_manager
        cls.user = mock_user

    @mock.patch.object(
        mock_pkce_flow_manager,
        "make_user_state",
        autospec=True,
        return_value=mock_state,
    )
    @mock.patch.object(
        mock_pkce_flow_manager,
        "make_user_code_verifier",
        autospec=True,
        return_value=mock_verifier,
    )
    @mock.patch.object(
        mock_pkce_flow_manager,
        "make_user_code_challenge",
        autospec=True,
        return_value=mock_challenge,
    )
    @mock.patch.object(mock_pkce_flow_manager, "store_user_secrets", autospec=True)
    def test_make_user_secrets(
        self,
        mock_store_secrets,
        mock_make_challenge,
        mock_make_verifier,
        mock_make_state,
    ):
        secrets = self.manager.make_user_secrets(user=mock_user)

        mock_make_state.assert_called_once_with(user=mock_user)
        mock_make_verifier.assert_called_once_with(user=mock_user)
        mock_make_challenge.assert_called_once_with(
            user=mock_user, code_verifier=mock_verifier
        )

        mock_store_secrets.assert_called_once_with(
            user=mock_user,
            state=mock_state,
            code_verifier=mock_verifier,
            code_challenge=mock_challenge,
        )

        self.assertEqual(secrets, (mock_state, mock_verifier, mock_challenge))

    def test_get_authorization_url(self):
        state, _, challenge = self.manager.make_user_secrets(user=self.user)

        authorization_url = self.manager.get_authorization_url(
            user=self.user, state=state, code_challenge=challenge
        )
        url_parts = urlparse(authorization_url)
        scheme = url_parts.scheme
        netloc = url_parts.netloc
        path = url_parts.path
        query = url_parts.query
        params = parse_qs(query)

        self.assertEqual(
            urlunparse((scheme, netloc, path, "", "", "",)),
            self.manager.base_url + self.manager.authorization_path,
        )
        self.assertEqual(params["client_id"], [self.manager.client_id])
        self.assertEqual(params["redirect_uri"], [self.manager.redirect_uri])
        self.assertEqual(params["response_type"], [self.manager.response_type])
        self.assertEqual(params["state"], [state])
        self.assertEqual(params["scope"], [self.manager.scope])
        self.assertEqual(params["code_challenge"], [challenge])
        self.assertEqual(
            params["code_challenge_method"], [self.manager.code_challenge_method],
        )

    @mock.patch.object(mock_pkce_flow_manager, "retrieve_user_state", autospec=True)
    @mock.patch.object(
        mock_pkce_flow_manager, "retrieve_user_code_challenge", autospec=True
    )
    def test_get_authorization_url_for_stored_secrets(
        self, mock_retrieve_challenge, mock_retrieve_state
    ):
        state, _, challenge = self.manager.make_user_secrets(user=self.user)
        mock_retrieve_state.return_value = state
        mock_retrieve_challenge.return_value = challenge

        authorization_url = self.manager.get_authorization_url(user=self.user)

        mock_retrieve_state.assert_called_once_with(self.user)
        mock_retrieve_challenge.assert_called_once_with(self.user)

        url_parts = urlparse(authorization_url)
        query = url_parts.query
        params = parse_qs(query)
        self.assertEqual(params["state"], [state])
        self.assertEqual(params["code_challenge"], [challenge])

    @mock.patch.object(
        mock_pkce_flow_manager,
        "get_authorization_url_extra_query_params",
        autospec=True,
        return_value={
            "code_challenge_method": "plain",
            "response_mode": "fragment",
            "nonce": "Andy, Duke of York",
        },
    )
    def test_get_authorization_url_with_extra_query_params(self, mock_get_extra_params):
        state, _, challenge = self.manager.make_user_secrets(user=self.user)

        authorization_url = self.manager.get_authorization_url(
            user=self.user, state=state, code_challenge=challenge
        )

        url_parts = urlparse(authorization_url)
        query = url_parts.query
        params = parse_qs(query)
        self.assertEqual(params["code_challenge_method"], ["plain"])
        self.assertEqual(params["response_mode"], ["fragment"])
        self.assertEqual(params["nonce"], ["Andy, Duke of York"])

    @mock.patch.object(
        mock_pkce_flow_manager, "check_user_state", autospec=True, return_value=True,
    )
    @mock.patch.object(
        mock_pkce_flow_manager,
        "retrieve_user_code_verifier",
        autospec=True,
        return_value=mock_verifier,
    )
    def test_request_access_token(self, mock_retrieve_verifier, mock_check_state):
        # patch 'requests.Session' with a mock session
        # mount a mock adapter on the session
        # ensure no other adapter could ever be mounted on the session
        with requests.Session() as mock_session, mock.patch(
            "requests.Session", autospec=True, return_value=mock_session
        ):
            mock_adapter = requests_mock.Adapter()
            expected_response_json = {
                "access_token": "an access token",
                "created_at": 1618201369,
                "refresh_token": "a refresh token",
                "scope": "read_api",
                "token_type": "Bearer",
            }
            mock_adapter.register_uri(
                "POST",
                self.manager.base_url + self.manager.token_fetch_path,
                json=expected_response_json,
            )
            mock_session.mount("https://", mock_adapter)
            mock_session.mount = lambda prefix, adapter: None

            resp = self.manager.fetch_access_token(
                user=self.user, state=mock_state, code=mock_oauth_code
            )

            self.assertEqual(resp.json(), expected_response_json)
            mock_check_state.assert_called_once_with(user=self.user, state=mock_state)
            mock_retrieve_verifier.assert_called_once_with(self.user)

            posted_payload = resp.request.json()
            self.assertEqual(posted_payload["client_id"], self.manager.client_id)
            self.assertEqual(
                posted_payload["client_secret"], self.manager.client_secret
            )
            self.assertEqual(posted_payload["code"], mock_oauth_code)
            self.assertEqual(posted_payload["grant_type"], self.manager.grant_type)
            self.assertEqual(posted_payload["redirect_uri"], self.manager.redirect_uri)
            self.assertEqual(posted_payload["code_verifier"], mock_verifier)

    @mock.patch.object(
        mock_pkce_flow_manager, "check_user_state", autospec=True, return_value=False,
    )
    def test_request_access_token_fails_for_forged_state(self, mock_check_state):
        with self.assertRaises(StateForgeryError):
            self.manager.fetch_access_token(
                user=self.user, state=mock_state, code=mock_oauth_code
            )

        mock_check_state.assert_called_once_with(user=self.user, state=mock_state)

    @mock.patch.object(
        mock_pkce_flow_manager, "check_user_state", autospec=True, return_value=True,
    )
    @mock.patch.object(
        mock_pkce_flow_manager,
        "retrieve_user_code_verifier",
        autospec=True,
        return_value=mock_verifier,
    )
    def test_request_access_token_fails_for_response_with_non_200_status_code(
        self, mock_retrieve_verifier, mock_check_state
    ):
        with requests.Session() as mock_session, mock.patch(
            "requests.Session", autospec=True, return_value=mock_session
        ):
            mock_adapter = requests_mock.Adapter()
            mock_adapter.register_uri(
                "POST",
                self.manager.base_url + self.manager.token_fetch_path,
                status_code=400,
            )
            mock_session.mount("https://", mock_adapter)
            mock_session.mount = lambda prefix, adapter: None

            with self.assertRaises(PKCEFlowError) as cm:
                self.manager.fetch_access_token(
                    user=self.user, state=mock_state, code=mock_oauth_code
                )

            exception = cm.exception
            self.assertIsNotNone(exception.response)
            self.assertIsNotNone(exception.request)
            self.assertIsInstance(exception.response, requests.Response)
            self.assertEqual(exception.response.request, exception.request)

    @mock.patch.object(
        mock_pkce_flow_manager, "check_user_state", autospec=True, return_value=True,
    )
    @mock.patch.object(
        mock_pkce_flow_manager,
        "retrieve_user_code_verifier",
        autospec=True,
        return_value=mock_verifier,
    )
    def test_request_access_token_fails_for_request_exception(
        self, mock_retrieve_verifier, mock_check_state
    ):
        with requests.Session() as mock_session, mock.patch(
            "requests.Session", autospec=True, return_value=mock_session
        ):
            mock_adapter = requests_mock.Adapter()
            mock_adapter.register_uri(
                "POST",
                self.manager.base_url + self.manager.token_fetch_path,
                exc=requests.exceptions.RequestException,
            )
            mock_session.mount("https://", mock_adapter)
            mock_session.mount = lambda prefix, adapter: None

            with self.assertRaises(PKCEFlowError) as cm:
                self.manager.fetch_access_token(
                    user=self.user, state=mock_state, code=mock_oauth_code
                )


class TestImproperlyConfiguredPKCEFlowManager(TestCase):
    def test_cannot_instantiate_improperly_configured_manager(self):
        with self.assertRaises(ImproperlyConfigured) as cm:
            MockImproperlyConfiguredPKCEFlowManager()

        error_message = str(cm.exception)
        self.assertIn("client_id", error_message)
        self.assertIn("client_secret", error_message)
        self.assertIn("redirect_uri", error_message)
        self.assertIn("base_url", error_message)
        self.assertIn("scope", error_message)
