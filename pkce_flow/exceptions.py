class BasePKCEFlowError(Exception):
    pass


class PKCEFlowError(BasePKCEFlowError):
    def __init__(self, *args, **kwargs):
        self.response = kwargs.pop("response", None)
        self.request = kwargs.pop("request", None)
        super().__init__()


class ImproperlyConfigured(BasePKCEFlowError):
    pass


class StateForgeryError(BasePKCEFlowError):
    pass
